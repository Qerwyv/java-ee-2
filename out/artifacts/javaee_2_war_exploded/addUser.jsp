<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: Arkenstone
  Date: 16.10.2019
  Time: 13:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add user</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
</head>
<body>
<form action="AddUser">
    <div class="form-row center justify-content-center">
        <div class="col-md-4 mb-3">
            <label for="validationServer01"><br> Name</label>
            <c:if test="${error == null}">
                <input autofocus type="text" class="form-control" name="name" id="validationServer01"
                       placeholder="Name"
                       value="" required>
            </c:if>
            <c:if test="${error != null}">
                <input type="text" class="form-control" name="name" id="validationServer01"
                       placeholder="Name"
                       value="${name}" required>
            </c:if>
        </div>
        <br>
        <div class="col-md-4 mb-3">
            <label for="validationServer02"><br>Phone number (with country code)</label>
            <c:if test="${error == null}">
                <input type="text" class="form-control" name="phoneNumber" pattern="[+][0-9]{7,15}"
                       id="validationServer02"
                       placeholder="Phone number (e.g. +380951234567)" required>
            </c:if>
            <c:if test="${error != null}">
                <input autofocus type="text" class="form-control is-invalid" name="phoneNumber"
                       pattern="[+][0-9]{7,15}"
                       id="validationServer02"
                       placeholder="Phone number" value="${phoneNumber}" required>
                <div class="invalid-feedback">
                    This phone number is already registered
                </div>
            </c:if>
        </div>
    </div>
    <br>
    <div style="text-align: center;">
        <input type="submit" class="btn btn-outline-primary" formmethod="post" value="Add">
    </div>
</form>
</body>
</html>
