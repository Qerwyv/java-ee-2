<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="userForEdit" scope="session" type="model.User"/>
<%@ page import="model.User" %>
<%@ page import="model.Users" %>

<%--
  Created by IntelliJ IDEA.
  User: Arkenstone
  Date: 14.10.2019
  Time: 3:48
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit user</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
</head>
<body>
<form action="EditUser">
    <div class="form-row center justify-content-center">
        <div class="col-md-4 mb-3">
            <label for="validationServer01">Name</label>
            <c:if test="${error == null}">
                <input autofocus type="text" class="form-control" name="name" id="validationServer01" placeholder="Name"
                       value="${userForEdit.name}" required>
            </c:if>
            <c:if test="${error != null}">
                <input type="text" class="form-control" name="name" id="validationServer01" placeholder="Name"
                       value="${userForEdit.name}" required>
            </c:if>
        </div>
        <br>
        <div class="col-md-4 mb-3">
            <label for="validationServer02">Phone number</label>
            <c:if test="${error == null}">
                <input type="text" class="form-control" name="phoneNumber" pattern="[+][0-9]{7,15}"
                       id="validationServer02"
                       placeholder="Phone number" value="${userForEdit.phoneNumber}" required>
            </c:if>
            <c:if test="${error != null}">
                <input autofocus type="text" class="form-control is-invalid" name="phoneNumber" pattern="[+][0-9]{7,15}"
                       id="validationServer02"
                       placeholder="Phone number" value="${incorrectPhoneNumber}" required>
                <div class="invalid-feedback">
                    This phone number is already registered
                </div>
            </c:if>
        </div>
        <br>
    </div>
    <br>
    <div style="text-align: center;">
        <input type="submit" class="btn btn-outline-primary" value="Edit">
    </div>
</form>
</body>
</html>
