package servlet;

import model.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/mylist")
public class ListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        session.setMaxInactiveInterval(86400);
        session.setAttribute("listOfUsers", Users.getListOfUsersStatic());
        if (!req.getParameterMap().isEmpty()) {
            String username = req.getParameter("username");
            String password = req.getParameter("password");
            if (username.equals("admin") && password.equals("service")) { //success
                session.setAttribute("login", "success");
                Users.readFromXml();
                session.setAttribute("listOfUsers", Users.getListOfUsersStatic());
                req.getRequestDispatcher("list.jsp").forward(req, resp);
                resp.setHeader("Refresh", "0;mylist");
            } else {
                resp.setHeader("Refresh", "0;redirect-to-login-page.jsp");
            }
        } else if (session.getAttribute("login") == null) {
            resp.sendRedirect("redirect-to-login-page.jsp");
        } else if (session.getAttribute("login").equals("success")) {
            session.removeAttribute("error");
            resp.sendRedirect("list.jsp");
        }
    }
}

