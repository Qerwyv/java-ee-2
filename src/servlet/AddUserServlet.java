package servlet;

import model.User;
import model.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBException;
import java.io.IOException;

@WebServlet("/AddUser")
public class AddUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        session.setMaxInactiveInterval(86400);
        if (req.getParameterMap().containsKey("name") && req.getParameterMap().containsKey("phoneNumber")) {
            String name = req.getParameter("name");
            String phoneNumber = req.getParameter("phoneNumber");
            req.setAttribute("name", name);
            req.setAttribute("phoneNumber", phoneNumber);
            for (User user : Users.getListOfUsersStatic()) {
                if (user.getPhoneNumber().equals(phoneNumber)) {
                    session.setAttribute("error", "number exists");
                    req.getRequestDispatcher("addUser.jsp").forward(req, resp);
                    resp.sendRedirect("addUser.jsp");
                    return;
                }
            }
            User newUser = new User(name, phoneNumber);
            Users users = new Users();
            users.getListOfUsers().add(newUser);
            try {
                Users.saveToXML(users);
                session.removeAttribute("error");
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            resp.sendRedirect("mylist");
            return;
        }
        req.getRequestDispatcher("addUser.jsp").forward(req, resp);
    }
}
