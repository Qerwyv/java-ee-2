package servlet;

import model.User;
import model.Users;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.IOException;

@WebServlet("/DeleteUser")
public class DeleteUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int userId = Integer.parseInt(req.getParameter("id"));
        User userForDelete = Users.getUserById(userId);
        if (userForDelete != null) {
            Users.getListOfUsersStatic().remove(userForDelete);
            Users newListAfterDeleting = new Users(Users.getListOfUsersStatic());
            try {
                Users.saveToXML(newListAfterDeleting);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            resp.setHeader("Refresh", "0;mylist");
        }
    }
}
