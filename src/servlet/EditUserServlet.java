package servlet;

import model.User;
import model.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBException;
import java.io.IOException;

@WebServlet("/EditUser")
public class EditUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //Writer writer = response.getWriter();
        HttpSession session = req.getSession();
        session.setMaxInactiveInterval(86400); //24 hours
        User userForEdit;
        if (req.getParameterMap().containsKey("id")) {
            int userId = Integer.parseInt(req.getParameter("id"));
            userForEdit = Users.getUserById(userId);
            session.setAttribute("userForEdit", userForEdit);
            //User user = (User) req.getAttribute("userForEdit");
            //System.out.println("user = " + user);
            //System.out.println("userForEdit = " + req.getAttribute("userForEdit"));
            req.getRequestDispatcher("editUser.jsp").forward(req, resp);
        } else if (req.getParameterMap().containsKey("name") && req.getParameterMap().containsKey("phoneNumber")) {
            String name = req.getParameter("name");
            String phoneNumber = req.getParameter("phoneNumber");
            userForEdit = (User) session.getAttribute("userForEdit");
            for (User user : Users.getListOfUsersStatic()) {
                if (user.getPhoneNumber().equals(phoneNumber) && (user.getId()!= userForEdit.getId())) {
                    System.out.println("error");
                    req.setAttribute("incorrectPhoneNumber", phoneNumber);
                    session.setAttribute("error", "number exists");
                    req.getRequestDispatcher("editUser.jsp").forward(req, resp);
                    resp.sendRedirect("editUser.jsp");
                    return;
                }
            }
            userForEdit = (User) session.getAttribute("userForEdit");
            userForEdit.setName(name);
            userForEdit.setPhoneNumber(phoneNumber);
            System.out.println("attrNames = " + session.getAttributeNames());
            System.out.println(userForEdit);
            try {
                Users.replaceUser(userForEdit);
                session.removeAttribute("error");
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            resp.sendRedirect("mylist");
//            User user = (User) req.getAttribute("userForEdit");
//            System.out.println("phoneNumber = " + user.getPhoneNumber());
//            user.setName(name);
//            user.setPhoneNumber(phoneNumber);
//            System.out.println("phoneNumber = " + user.getPhoneNumber());

        }

    }
}